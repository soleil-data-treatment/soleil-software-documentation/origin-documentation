# Aide et ressources de Origin pour Synchrotron SOLEIL

[<img src="https://d2mvzyuse3lwjc.cloudfront.net/images/header_logo.png" width="250"/>](https://www.originlab.com/Origin)

## Résumé

- Traitement des données et mise en forme
- Privé

## Sources

- Code source: Privé
- Documentation officielle: https://www.originlab.com/Origin

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: Fichier ascii
- en sortie: Ascii,  jpg,  origin
- sur un disque dur,  sur la Ruche
